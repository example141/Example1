import 'package:flutter/material.dart';
import 'package:lesson2/home_screen.dart';
import 'package:lesson2/login_screen.dart';
import 'generated/l10n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      supportedLocales: S.delegate.supportedLocales,
      routes: {
        '/': (context) => AuthWidget(),
        '/home_screen': (context) =>
            const MyHomePage(title: 'Flutter Demo Home Page'),
      },
      locale: const Locale('ru', 'RU'),
      initialRoute: '/',
    );
  }
}
